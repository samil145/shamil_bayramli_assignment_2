using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_shit : MonoBehaviour
{
    [SerializeField]
    TMP_Text coinText;

    [SerializeField]
    TMP_Text scoreText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        coinText.text = "Coin: " + Controller.coinCount.ToString();
        scoreText.text = "Score: " + Controller.scoreCount.ToString();
    }


}
