using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    CharacterController c;
    float dx, dz;
    [SerializeField]
    float speed;

    [SerializeField]
    float gravity;

    [SerializeField]
    float jumpHeigt;

    [SerializeField]
    GameObject prefab;

    static internal int coinCount;

    static internal int scoreCount;

    bool jump;

    Vector3 movement = Vector3.zero;

    void Start()
    {
        c = GetComponent<CharacterController>();
        speed = 5;
        gravity = 9.81f;
        jumpHeigt = 2;
        for (int i = 0; i < 5; i++)
        {
            Instantiate(prefab,new Vector3(Random.Range(-20,20),0, Random.Range(-20, 15)),Quaternion.Euler(0,Random.Range(-90,90),0));
        }
    }

    void Update()
    {
        //input
        dx = Input.GetAxis("Horizontal");
        dz = Input.GetAxis("Vertical");
        if (!jump && Input.GetKeyDown(KeyCode.Space))
            jump = true;
    }

    private void FixedUpdate()
    {
        movement = (transform.right.normalized * dx + transform.forward.normalized * dz).normalized * speed * Time.fixedDeltaTime;
        
        //movement = new Vector3(transform.right.normalized.x * dx, 0, transform.forward.normalized.z * dz) * speed * Time.fixedDeltaTime;
        
        //jumping logic
        if (jump)
        {
            movement.y = jumpHeigt;
            jump = false;
        }
        
        //gravity logic
        if (jump && c.isGrounded)
            movement.y = 0;
        else
            movement.y -= gravity;
        
        
        c.Move(movement);
        
        var mousepos = Input.mousePosition;
        var capsulePos = Camera.main.WorldToScreenPoint(transform.position);
        var dir = capsulePos - mousepos;
        
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(-angle - 90, Vector3.up);
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.name == "Sphere")
        {
            hit.gameObject.GetComponent<Rigidbody>().AddForce((hit.gameObject.transform.position - transform.position + new Vector3(0,0.2f,0)) * 1000);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            coinCount++;
            other.gameObject.SetActive(false);
        }

        if (other.name == "Plane(1)")
        {
            scoreCount++;
        }
    }
}
