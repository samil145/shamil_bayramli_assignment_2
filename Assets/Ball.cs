using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Cube (4)")
        {
            Controller.scoreCount++;
        }

        switch (collision.gameObject.name)
        {
            case "Front":
                Debug.Log("Front Wall");
                break;
            case "Back":
                Debug.Log("Back Wall");
                break;
            case "Right":
                Debug.Log("Right Wall");
                break;
            case "Left":
                Debug.Log("Left Wall");
                break;
        }
    }

}
